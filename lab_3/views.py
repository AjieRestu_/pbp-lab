from django.contrib.auth import login
from django.shortcuts import redirect, render
from datetime import datetime,date
from lab_1.models import Friend
from .forms import FriendForm
from django.contrib.auth.decorators import login_required
# Create your views here.
@login_required(login_url="/admin/login/")
def index(request):
    friends = Friend.objects.all()
    response = {'friends' : friends}
    return render(request, 'lab3_index.html',response)

@login_required(login_url="/admin/login/")
def add_friends(request):
    context = {}
    form = FriendForm(request.POST, request.FILES or None)
    if form.is_valid():
        # save the form data to model
        form.save()
        return redirect("/lab-3")
  
    context['form']= form
    return render(request, "lab3_form.html", context)