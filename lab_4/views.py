from django.shortcuts import render,redirect
from lab_2.models import Note
from lab_4.forms import NoteForm
from django.contrib import messages

# Create your views here.
def index(request):
    data = Note.objects.all()
    response = {'data':data}
    return render(request, 'lab4_index.html',response)

def note_list(request):
    data = Note.objects.all()
    response = {'data':data}
    return render(request, 'lab4_note_list.html',response)



def add_note(request):
    context = {}
    form = NoteForm(request.POST, request.FILES or None)
    if form.is_valid():
        # save the form data to model
        form.save()
        messages.success(request,("Your Message Has Been Submitted!"))
        return redirect("/lab-4")
  
    context['form']= form
    return render(request, "lab4_form.html", context)