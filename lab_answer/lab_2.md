No 1 Answer :
    1.XML data is typeless while JSON object has a type
    2.Namespaces are not supported by JSON, however they are supported by XML.
    3.JSON has no display capabilities whereas XML offers the capability to display data.
    4.XML supports various encoding formats while JSON supports only UTF-8 encoding.

Source : https://www.guru99.com/json-vs-xml-difference.html

No 2 Answer:
    1.HTML is not Case sensitive while XML is Case sensitive.
    2.HTML stands for Hyper Text Markup Language while XML stands for extensible Markup Language.
    3.HTML can ignore small errors while XML does not allow errors.
    4.HTML is static while XML is dynamic.