import 'package:flutter/material.dart';
import './screens/aboutus.dart';
import './screens/login.dart';
void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Lab 6 PBP',
      theme: ThemeData(
        primarySwatch: Colors.lime,
        fontFamily: 'MADE-Sunflower'
        
      ),
      home: const MyHomePage(),
      debugShowCheckedModeBanner: false,
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({Key? key}) : super(key: key);

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".



  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      endDrawer: Drawer(
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            const DrawerHeader(
              decoration: BoxDecoration(
                color: Color.fromRGBO(195, 197, 173,1),
              ),
              child: Text(
                'Menu',
                style: TextStyle(
                  fontWeight: FontWeight.w300,
                ),
              ),
            ),

            // for item 1
            ListTile(
              title: const Text('Home'),
              onTap: () {Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => MyApp(),
                  ),
                );
              },
            ),

            // for item 2
            ListTile(
              title: const Text('About Us'),
              onTap: () {Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => AboutUs(),
                  ),
                );
                },
            ),
             ListTile(
              title: const Text('Login'),
              onTap: () {Navigator.push(
                  context,
                  MaterialPageRoute(
                    builder: (context) => Login(),
                  ),
                );},
            ),
          ],
        ),
      ),
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text('Stotes',
        style: TextStyle(fontFamily: 'MADE-Sunflower',
            fontSize: 30,),),
        backgroundColor: Color.fromRGBO(195, 197, 173,1),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          
          children: <Widget>[
           
            Image.network('https://very-awesome-projectmanager.herokuapp.com/static/img/task-view.png',width:900,height:300),
            Container(
            padding: EdgeInsets.fromLTRB(25, 60, 25, 0),
            child: Text('Working from home has never been this easy.',style: TextStyle(fontSize: 30,color: Colors.black,fontFamily: 'MADE-Sunflower'),)
            ),
            Container(
            padding: EdgeInsets.fromLTRB(25,10, 25, 60),
            child: Text('Feel the benefits and be more productive with us.',style: TextStyle(fontSize: 30,color: Colors.black,fontFamily: 'MADE-Sunflower'),)
            ),
            SizedBox(
                        width: 100,
                        height: 35,
                        child: RaisedButton(
                  child: Text(
                    'Login',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      fontFamily: 'MADE-Sunflower',
                      color: Colors.black,
                    ),
                  ),
                  onPressed: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                        builder: (context) => Login(),
                      ),
                    );
                  },
                  color: Color.fromRGBO(195, 197, 173,1),
                ),
                    ),

          ],
        ),
      ),
       // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
