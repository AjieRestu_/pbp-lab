import 'package:flutter/material.dart';

class Login extends StatelessWidget {
  const Login({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Login',style: TextStyle(color: Colors.black,fontFamily: 'MADE-Sunflower'),),
        backgroundColor: Color.fromRGBO(195, 197, 173,1),
      ),
      body: Center(
        child: Text('Login',style: TextStyle(fontSize: 30,color: Colors.black,fontFamily: 'MADE-Sunflower'),),
      ),
    );
  }
}