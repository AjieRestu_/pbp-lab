import 'package:flutter/material.dart';
import '../main.dart';
import '../screens/aboutus.dart';

class Login extends StatelessWidget {
    const Login({Key? key}) : super(key : key,);
    @override
    Widget build(BuildContext context) {
		final _formKey = GlobalKey<FormState>();

        return Scaffold(
            appBar: AppBar(
                title: const Text("Stotes",
                            style: TextStyle(
                                    fontWeight: FontWeight.bold
                            ),
                    ),
                backgroundColor: Color.fromRGBO(195, 197, 173,1),
            ),

            body: Form(
				key: _formKey,
				child: Column(
					children: <Widget> [
						Container (
							decoration: BoxDecoration(
								color: Color.fromRGBO(195, 197, 173,1),
								borderRadius: BorderRadius.circular(5),
							),

							margin: const EdgeInsets.only(top: 100),
							padding: const EdgeInsets.fromLTRB(68,18,68,18),
							child: const Text("Login",
								style: TextStyle(
									fontWeight: FontWeight.bold,
									fontSize: 25,
									color: Colors.black,
								),
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Username",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter username!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 10),

						Container (
							padding: const EdgeInsets.fromLTRB(50,0,50,0),
							child: TextFormField(
								decoration: const InputDecoration(
									hintText: "Password",
									border: OutlineInputBorder(),
								),
								validator: (value) {
									if (value!.isEmpty) {
										return "Enter password!";
									}
								return null;
								},
							),
						),

						const SizedBox(height: 15),

						SizedBox(
							width: 100,
							height: 40,
							child: RaisedButton(
								onPressed: () {
									if (_formKey.currentState!.validate()) {
										Navigator.push(
											context,
											MaterialPageRoute(builder: (context) => const MyHomePage()),
										);
									}
								},
								child: const Text("Login",
									style: TextStyle(
										fontWeight: FontWeight.bold,
									),
								),
                color: Color.fromRGBO(195, 197, 173,1),
							),
						),
					],
				),
			),

            endDrawer: Drawer(
                child: ListView(
                    children: <Widget> [
						SizedBox (
							height: 64,
							child: DrawerHeader(
								decoration: BoxDecoration(
									color: Color.fromRGBO(195, 197, 173,1),
								),

								child: const Text('Menu',
                                    style: TextStyle(
                                        fontWeight: FontWeight.bold,
                                        color: Colors.black,
                                        fontSize: 25,
                                    )
                                ),
							),
						),

                        // for item 1
                        ListTile(
                            title: const Text('Home',
								style: TextStyle(
									fontWeight: FontWeight.bold,
									),
								),
                                onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => const MyHomePage()),
                                    );
                                },
                        ),

                        ListTile(
                            title: const Text('About',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    ),
                                ),
                                onTap: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(builder: (context) => const AboutUs()),
                                    );
                                },
                        ),
                        ListTile(
                            title: const Text('Login',
                            style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    ),
                                ),
                              
                            onTap: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(builder: (context) => const Login(),
                                  ),
                               );},
                        ),
                    ],
                ),
            ),
        );
    }
}