import 'package:flutter/material.dart';

class AboutUs extends StatelessWidget {
  const AboutUs({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('About Us',style: TextStyle(color: Colors.black,fontFamily: 'MADE-Sunflower'),),
        backgroundColor: Color.fromRGBO(195, 197, 173,1),
      ),
      body: Column(
        children:<Widget>[
          Container(
            padding: EdgeInsets.fromLTRB(25, 60, 25, 10),
            child: Text('About Us',style: TextStyle(fontSize: 30,color: Colors.black,fontFamily: 'MADE-Sunflower'),)
            ),
          const Text(
              'We are a team of students wanting a better workspace management system without all the confusing features. Inspired by the simpleness of using sticky notes for day-to-day reminders, we wanted to bring that to your screens by introducing an intuitive website that helps you keep track of your daily tasks.',textAlign: TextAlign.center,
              style: TextStyle(fontFamily: 'MADE-Sunflower',fontSize: 25,),
            ),
        
            ]
        ),

    );
  }
}